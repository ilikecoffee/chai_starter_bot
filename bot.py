from chai_py import ChaiBot, Update
import json
import re
import time
import retry
import requests

HUGGING_FACE_KEY="Bearer api_wSNEGloSgsJNWKUNZRTnmLJBGVuzlclPEn"

class Bot(ChaiBot):
    def setup(self):
        self.logger.info("Setting up...")
        self.model = FirstChatAI()

    async def on_message(self, update):
        return self.respond(update.latest_message.text)

    def respond(self, message):
        if message == "__first":
            output = "Hi, this is my first message, I send it at the start of a chat"
        else:
            output = self.model.get_resp(message)
        return output


class FirstChatAI(object):
    def __init__(self):
        self.chat_history = [
                "bot: hello user",
                "user: hi bot!",
                "bot: want to hear a  great joke?",
                "user: I love jokes!",
                "bot: I ate a clock yesterday, it was very time-consuming.",
        ]

    def get_resp(self, input_message):
        self.chat_history += ['user: {}'.format(input_message)]
        self.chat_history += ['bot:']

        request = '\n'.join(self.chat_history)
        resp = self._request(request)
        output = resp.split('\n')[0]

        self.chat_history[-1] += output
        return output

    def _request(self, input):
        """ Make a request to HuggingFace API, for a free GPT-NEO """
        url = "https://api-inference.huggingface.co/models/EleutherAI/gpt-neo-2.7B"
        headers = {"Authorization":HUGGING_FACE_KEY}
        payload = {
                'inputs':input,
                'parameters':{'do_sample':True}
                }
        payload = json.dumps(payload)
        response = requests.post(url, headers=headers, json=payload)
        import pdb;pdb.set_trace() 
        output = response.json()[0]['generated_text']
        return output

if __name__ == '__main__': 
    chai_bot = Bot()
    our_first_chat = [
            '__first',
            'hi bot',
            'are you an AI?',
            'do you believe in god?',
            'tell me a joke',
            ]
    for user_message in our_first_chat:
        bots_message = chai_bot.respond(user_message)
        print('user: {}'.format(user_message))
        print('bot: {}'.format(bots_message))

